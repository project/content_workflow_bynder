<?php

namespace Drupal\Tests\content_workflow_bynder\Kernel;

use Drupal\content_workflow_bynder_test\MockData;
use Drupal\migrate_plus\Entity\Migration;
use Symfony\Component\Yaml\Yaml;

/**
 * Class ConfigCreatorTest.
 *
 * @group content_workflow_bynder
 */
class ConfigCreatorTest extends CwbMigrateTestBase {

  const CONFIG_NAMES_CONFIG_CREATE_TEST = [
    '821317' => [
      'migrate_plus.migration.86701_821317_node_article_en' => 'test1.1.yml',
      'migrate_plus.migration.86701_821317_node_article_hu' => 'test1.2.yml',
      'migrate_plus.migration.86701_821317_node_article_engb' => 'test1.3.yml',
    ],
    '819462' => [
      'migrate_plus.migration.86701_819462_node_simple_test_type_en' => 'test1.1.yml',
      'migrate_plus.migration.86701_819462_node_simple_test_type_hu' => 'test1.2.yml',
      'migrate_plus.migration.86701_819462_paragraph_paragraph_test_type_en' => 'paragraph_test_en.yml',
      'migrate_plus.migration.86701_819462_paragraph_paragraph_test_type_hu' => 'paragraph_test_hu.yml',
    ],
  ];

  const CONFIG_KEYS_TO_CHECK = [
    'langcode',
    'status',
    'dependencies',
    'id',
    'label',
    'source',
    'process',
    'destination',
    'migration_dependencies',
  ];

  /**
   * Test config creation.
   */
  public function testConfigCreate() {
    foreach (self::CONFIG_NAMES_CONFIG_CREATE_TEST as $templateId => $testFiles) {
      /** @var \Drupal\content_workflow_bynder\Entity\MappingInterface $mapping */
      $mapping = MockData::getSpecificMapping($templateId);
      $mappingData = unserialize($mapping->getData());

      /** @var \Drupal\content_workflow_bynder\MigrationDefinitionCreator $creator */
      $creator = \Drupal::service('content_workflow_bynder.migration_creator');
      $creator
        ->setMapping($mapping)
        ->setMappingData($mappingData)
        ->createMigrationDefinition();
      $configFactory = \Drupal::configFactory();

      foreach ($testFiles as $configName => $testFile) {
        $configCreatedByService = $configFactory->getEditable($configName);
        if (!file_exists(__DIR__ . "/../../modules/content_workflow_bynder_test/test_definition/$templateId/" . $testFile)) {
          $config = \Drupal::configFactory()->get($configName);

          file_put_contents(__DIR__ . "/../../modules/content_workflow_bynder_test/test_definition/$templateId/" . $testFile, Yaml::dump($config->get()));
        }
        $testYml = file_get_contents(__DIR__ . "/../../modules/content_workflow_bynder_test/test_definition/$templateId/" . $testFile);
        if (!$testYml) {
          continue;
        }
        $parsedYml = Yaml::parse($testYml);
        $expected = Migration::create($parsedYml);
        foreach (self::CONFIG_KEYS_TO_CHECK as $key) {
          $actual = $configCreatedByService->get($key);
          self::assertEquals($expected->get($key), $actual, "Failed assertion template: $templateId in file: $testFile at key: $key");
        }
      }
    }
  }

}
