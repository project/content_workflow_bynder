<?php

namespace Drupal\Tests\content_workflow_bynder\Kernel;

use Drupal\content_workflow_bynder\MetatagQuery;
use Drupal\content_workflow_bynder_test\MockData;
use Drupal\content_workflow_bynder_test\MockDrupalContentWorkflowBynderClient;
use Drupal\KernelTests\KernelTestBase;

/**
 * Base class for import kernel tests.
 */
abstract class CwbImportTestBase extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'node', 'text', 'field', 'user', 'image', 'file', 'taxonomy', 'language',
    'content_translation', 'paragraphs', 'entity_reference_revisions', 'system',
    'metatag', 'menu_ui', 'menu_link_content', 'link', 'content_workflow_bynder', 'content_workflow_bynder_test',
    'migrate', 'migrate_plus', 'migrate_tools', 'token',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installSchema('node', 'node_access');
    $this->installEntitySchema('node');
    $this->installEntitySchema('file');
    $this->installSchema('file', ['file_usage']);
    $this->installEntitySchema('taxonomy_term');
    $this->installEntitySchema('paragraph');
    $this->installEntitySchema('user');
    $this->installEntitySchema('menu_link_content');
    $this->installConfig(['content_workflow_bynder_test']);
    MockData::$drupalRoot = $this->getDrupalRoot();
    /** @var \Drupal\taxonomy\Entity\Term[] $terms */
    $terms = MockData::createTaxonomyTerms();
    foreach ($terms as $term) {
      $term->save();
    }
  }

  /**
   * Get singleton MetatagQuery object.
   */
  public static function getMetatag() {
    static $metatag = NULL;
    if ($metatag === NULL) {
      $metatag = new MetatagQuery(
        \Drupal::service('entity_field.manager')
      );
    }
    return $metatag;
  }

  /**
   * Get singleton Mock client.
   */
  public static function getClient() {
    static $client = NULL;
    if ($client === NULL) {
      $client = new MockDrupalContentWorkflowBynderClient(
        \Drupal::service('http_client')
      );
    }
    return $client;
  }

}
