<?php

namespace Drupal\content_workflow_bynder;

use GatherContent\DataTypes\Item;
use Drupal\content_workflow_bynder\Entity\Mapping;

/**
 * A static class to return and cache mapping entities.
 */
class MappingLoader {

  /**
   * Mappings.
   *
   * @var array
   */
  protected static $mappings = [];

  /**
   * Cache mappings when loading them.
   */
  public static function load(Item $cwbItem) {
    if (!isset(static::$mappings[$cwbItem->id])) {
      static::$mappings[$cwbItem->id] = static::getMapping($cwbItem);
    }
    return static::$mappings[$cwbItem->id];
  }

  /**
   * Return the mapping associated with the given Item.
   */
  public static function getMapping(Item $cwbItem) {
    $mappingId = \Drupal::entityQuery('content_workflow_bynder_mapping')
      ->accessCheck(TRUE)
      ->condition('project_id', $cwbItem->projectId)
      ->condition('template_id', $cwbItem->templateId)
      ->execute();

    if (empty($mappingId)) {
      throw new \Exception("Operation failed: Template not mapped.");
    }

    $mappingId = reset($mappingId);
    $mapping = Mapping::load($mappingId);

    if ($mapping === NULL) {
      throw new \Exception("No mapping found with id: $mappingId");
    }

    return $mapping;
  }

}
