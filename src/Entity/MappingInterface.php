<?php

namespace Drupal\content_workflow_bynder\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Content Workflow Mapping entities.
 */
interface MappingInterface extends ConfigEntityInterface {

  /**
   * Getter for ContentWorkflowBynder project ID property.
   *
   * @return int
   *   ContentWorkflowBynder project ID.
   */
  public function getGathercontentProjectId();

  /**
   * Getter for ContentWorkflowBynder project property.
   *
   * @return string
   *   ContentWorkflowBynder project name.
   */
  public function getGathercontentProject();

  /**
   * Getter for ContentWorkflowBynder template ID property.
   *
   * @return int
   *   ContentWorkflowBynder template ID.
   */
  public function getGathercontentTemplateId();

  /**
   * Getter for ContentWorkflowBynder template property.
   *
   * @return string
   *   ContentWorkflowBynder template name.
   */
  public function getGathercontentTemplate();

  /**
   * Getter for entity type machine name.
   *
   * @return string
   *   Entity type machine name.
   */
  public function getMappedEntityType();

  /**
   * Setter for entity type machine name.
   *
   * @param string $entity_type
   *   Entity type machine name.
   */
  public function setMappedEntityType($entity_type);

  /**
   * Getter for content type machine name.
   *
   * @return string
   *   Content type machine name.
   */
  public function getContentType();

  /**
   * Setter for content type machine name.
   *
   * @param string $content_type
   *   Content type machine name.
   */
  public function setContentType($content_type);

  /**
   * Getter for content type human name.
   *
   * @return string
   *   Content type human name.
   */
  public function getContentTypeName();

  /**
   * Setter for content type human name.
   *
   * @param string $content_type_name
   *   Content type human name.
   */
  public function setContentTypeName($content_type_name);

  /**
   * Getter for ContentWorkflowBynder template serialized object.
   *
   * @return string
   *   Serialized ContentWorkflowBynder template.
   */
  public function getTemplate();

  /**
   * Setter for ContentWorkflowBynder template serialized object.
   *
   * @param string $template
   *   Serialized ContentWorkflowBynder template.
   */
  public function setTemplate($template);

  /**
   * Getter for mapping data.
   *
   * @return string
   *   Serialized object of mapping.
   */
  public function getData();

  /**
   * Setter for mapping data.
   *
   * @param string $data
   *   Serialized object of mapping.
   */
  public function setData($data);

  /**
   * Setter for updated drupal property.
   *
   * @param string $updated_drupal
   *   Timestamp when was mapping updated.
   */
  public function setUpdatedDrupal($updated_drupal);

  /**
   * Validate if object is configured with mapping.
   *
   * @return bool
   *   Return TRUE if object has mapping, otherwise FALSE.
   */
  public function hasMapping();

  /**
   * Formatter for content type property.
   *
   * @return string
   *   If not empty return human name for content type, else return None string.
   */
  public function getFormattedContentType();

  /**
   * Formatter for entity type property.
   *
   * @return string
   *   If not empty return human name for entity type, else return None string.
   */
  public function getFormattedEntityType();

  /**
   * Formatter for updated drupal property.
   *
   * @return string
   *   If not empty return formatted date, else return string Never.
   */
  public function getFormatterUpdatedDrupal();

  /**
   * Getter for ContentWorkflowBynder migrations list.
   *
   * @return array
   *   Migration id list as array.
   */
  public function getMigrations();

}
