<?php

namespace Drupal\content_workflow_bynder\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Defines the Content Workflow Mapping entity.
 *
 * @ConfigEntityType(
 *   id = "content_workflow_bynder_mapping",
 *   label = @Translation("Content Workflow Mapping"),
 *   config_prefix = "content_workflow_bynder_mapping",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "uuid",
 *     "project_id",
 *     "project",
 *     "template_id",
 *     "template_name",
 *     "entity_type",
 *     "content_type",
 *     "content_type_name",
 *     "updated_drupal",
 *     "data",
 *     "template",
 *     "migration_definitions"
 *   }
 * )
 */
class Mapping extends ConfigEntityBase implements MappingInterface {

  use StringTranslationTrait;

  /**
   * The Content Workflow Mapping ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Content Workflow Project ID.
   *
   * @var int
   */
  protected $project_id;

  /**
   * The Content Workflow Project name.
   *
   * @var string
   */
  protected $project;

  /**
   * The Content Workflow Template ID.
   *
   * @var int
   */
  protected $template_id;

  /**
   * The Content Workflow Template name.
   *
   * @var string
   */
  protected $template_name;

  /**
   * Entity type machine name.
   *
   * @var string
   */
  protected $entity_type;

  /**
   * Content type machine name.
   *
   * @var string
   */
  protected $content_type;

  /**
   * Content type name.
   *
   * @var string
   */
  protected $content_type_name;

  /**
   * Timestamp of mapping update in Drupal.
   *
   * @var string
   */
  protected $updated_drupal;

  /**
   * Mapping data.
   *
   * @var string
   */
  protected $data;

  /**
   * Template during latest update.
   *
   * @var string
   */
  protected $template;

  /**
   * {@inheritdoc}
   */
  public function label() {
    return $this->t('%project: %template', ['%project' => $this->project, '%template' => $this->template_name]);
  }

  /**
   * {@inheritdoc}
   */
  public function getGathercontentTemplateId() {
    return $this->get('template_id');
  }

  /**
   * Set the template used in ContentWorkflowBynder.
   *
   * @param int $template_id
   *   The template id.
   */
  public function setGathercontentTemplateId($template_id) {
    $this->template_id = $template_id;
  }

  /**
   * {@inheritdoc}
   */
  public function getGathercontentProjectId() {
    return $this->get('project_id');
  }

  /**
   * Set the project id used in ContentWorkflowBynder.
   *
   * @param int $project_id
   *   The project id.
   */
  public function setGathercontentProjectId($project_id) {
    $this->project_id = $project_id;
  }

  /**
   * {@inheritdoc}
   */
  public function getGathercontentProject() {
    return $this->get('project');
  }

  /**
   * Set the project name used in ContentWorkflowBynder.
   *
   * @param string $project
   *   The name of the project.
   */
  public function setGathercontentProject($project) {
    $this->project = $project;
  }

  /**
   * {@inheritdoc}
   */
  public function getGathercontentTemplate() {
    return $this->get('template_name');
  }

  /**
   * Set the template name used in Content Workflow.
   *
   * @param string $template_name
   *   The name of the template.
   */
  public function setGathercontentTemplate($template_name) {
    $this->template = $template_name;
  }

  /**
   * {@inheritdoc}
   */
  public function getMappedEntityType() {
    return $this->get('entity_type');
  }

  /**
   * {@inheritdoc}
   */
  public function setMappedEntityType($entity_type) {
    $this->entity_type = $entity_type;
  }

  /**
   * {@inheritdoc}
   */
  public function getContentType() {
    return $this->get('content_type');
  }

  /**
   * {@inheritdoc}
   */
  public function setContentType($content_type) {
    $this->content_type = $content_type;
  }

  /**
   * {@inheritdoc}
   */
  public function getContentTypeName() {
    return $this->get('content_type_name');
  }

  /**
   * Set the content type name.
   *
   * {@inheritdoc}
   */
  public function setContentTypeName($content_type_name) {
    $this->content_type_name = $content_type_name;
  }

  /**
   * Get the date of the last update.
   *
   * @return string
   *   Userfriendly timestamp of the last update.
   */
  public function getUpdatedDrupal() {
    return $this->get('updated_drupal');
  }

  /**
   * {@inheritdoc}
   */
  public function setUpdatedDrupal($updated_drupal) {
    $this->updated_drupal = $updated_drupal;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormattedContentType() {
    $content_type = $this->get('content_type_name');
    if (!empty($content_type)) {
      return $content_type;
    }
    else {
      return $this->t('None');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getFormattedEntityType() {
    $entity_type = $this->get('entity_type');
    if (!empty($entity_type)) {
      return ucfirst($entity_type);
    }
    else {
      return $this->t('None');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getFormatterUpdatedDrupal() {
    $updated_drupal = $this->get('updated_drupal');
    if (!empty($updated_drupal)) {
      return \Drupal::service('date.formatter')
        ->format($updated_drupal, 'custom', 'M d, Y - H:i');
    }
    else {
      return $this->t('Never');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getTemplate() {
    return $this->get('template');
  }

  /**
   * {@inheritdoc}
   */
  public function setTemplate($template) {
    $this->template = $template;
  }

  /**
   * {@inheritdoc}
   */
  public function getData() {
    return $this->get('data');
  }

  /**
   * {@inheritdoc}
   */
  public function setData($data) {
    $this->data = $data;
  }

  /**
   * {@inheritdoc}
   */
  public function hasMapping() {
    return !empty($this->get('data'));
  }

  /**
   * {@inheritdoc}
   */
  public function getMigrations() {
    return $this->get('migration_definitions');
  }

  /**
   * {@inheritdoc}
   */
  public static function postDelete(EntityStorageInterface $storage, array $entities) {
    parent::postDelete($storage, $entities);

    // Do not delete migrations during an active config sync.
    if (\Drupal::isConfigSyncing()) {
      return;
    }

    // Delete the related migration definitions.
    $entityTypeManager = \Drupal::entityTypeManager();
    $migrationStorage = $entityTypeManager->getStorage('migration');

    foreach ($entities as $entity) {
      $migrationIds = $entity->getMigrations();

      if (!$migrationIds) {
        continue;
      }

      $migrations = $migrationStorage->loadMultiple($migrationIds);
      if ($migrations) {
        $migrationStorage->delete($migrations);
      }
    }
  }

}
