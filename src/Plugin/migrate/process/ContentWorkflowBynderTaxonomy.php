<?php

namespace Drupal\content_workflow_bynder\Plugin\migrate\process;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Perform custom value transformation.
 *
 * @MigrateProcessPlugin(
 *   id = "content_workflow_bynder_taxonomy"
 * )
 *
 * @code
 * taxonomy_field:
 *   plugin: content_workflow_bynder_taxonomy
 *   bundle: vid
 *   source: field
 * @endcode
 */
class ContentWorkflowBynderTaxonomy extends ProcessPluginBase implements ContainerFactoryPluginInterface {

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entityTypeManager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if ($value) {
      $taxonomy = $this->entityTypeManager
        ->getStorage('taxonomy_term')
        ->loadByProperties([
          'contentworkflowbynder_option_ids' => $value,
          'vid' => $this->configuration['bundle'],
        ]);

      if ($taxonomy) {
        $selected_options = array_keys($taxonomy);
        return reset($selected_options);
      }
    }

    return NULL;
  }

}
