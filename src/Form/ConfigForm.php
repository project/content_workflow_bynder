<?php

namespace Drupal\content_workflow_bynder\Form;

use Drupal\Core\Config\TypedConfigManagerInterface;
use GatherContent\GatherContentClientInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ConfigForm.
 *
 * @package Drupal\content_workflow_bynder\Form
 */
class ConfigForm extends ConfigFormBase {

  use StringTranslationTrait;

  /**
   * Content Workflow client.
   *
   * @var \Drupal\content_workflow_bynder\DrupalContentWorkflowBynderClient
   */
  protected $client;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, TypedConfigManagerInterface $typed_config_manager, GatherContentClientInterface $client) {
    parent::__construct($config_factory, $typed_config_manager);
    $this->client = $client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('config.typed'),
      $container->get('content_workflow_bynder.client')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('content_workflow_bynder.settings');
    $form['username'] = [
      '#type' => 'email',
      '#title' => $this->t('User Email Address'),
      '#description' => $this->t('This is the email address you use to login to Content Workflow. Your permissions will determine what accounts, projects and content is available.'),
      '#default_value' => $config->get('username'),
      '#required' => TRUE,
    ];
    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API key'),
      '#description' => Link::fromTextAndUrl($this->t('Click to find out where you can generate your API Key'), Url::fromUri('https://help.gathercontent.com/en/articles/369871-generating-an-api-key-the-api-documentation#generate-an-api-key')),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('api_key'),
      '#required' => TRUE,
    ];

    $form['actions']['#type'] = 'actions';

    if (!$form_state->isSubmitted()) {
      $account = $config->get('account');
      if (!empty($account)) {
        $account = unserialize($account);
        $account = array_pop($account);
        $form['current_account'] = [
          '#type' => 'html_tag',
          '#tag' => 'div',
          '#value' => $this->t('Current account is <strong>@account</strong>.', ['@account' => $account]),
        ];
      }
    }

    if ($form_state->isSubmitted()) {
      $data = $this->client->accountsGet();
      $accounts = [];

      if (!is_null($data['data'])) {
        foreach ($data['data'] as $account) {
          $accounts[$account->id] = $account->name;
        }

        $form['account'] = [
          '#type' => 'select',
          '#options' => $accounts,
          '#title' => $this->t('Select Content Workflow Account'),
          '#required' => TRUE,
          '#description' => $this->t('Multiple accounts will be listed if the Content Workflow
       user has more than one account. Please select the account you want to
       import and update content from.'),
        ];
      }
      $form['actions']['submit'] = [
        '#type' => 'submit',
        '#value' => (is_null($data)) ? $this->t('Verify') : $this->t('Save'),
        '#button_type' => 'primary',
      ];
    }
    else {
      $form['actions']['submit'] = [
        '#type' => 'submit',
        '#value' => (!empty($account) ? $this->t('Change Content Workflow Account') : $this->t('Verify')),
        '#button_type' => 'primary',
      ];
    }

    if (!empty($account)) {
      $form['actions']['reset'] = [
        '#type' => 'submit',
        '#value' => $this->t('Reset credentials'),
      ];
    }

    // By default, render the form using theme_system_config_form().
    $form['#theme'] = 'system_config_form';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $this->client->setEmail($form_state->getValue('username'));
    $this->client->setApiKey($form_state->getValue('api_key'));
    try {
      $this->client->accountsGet();
    }
    catch (\Exception $e) {
      $form_state->setErrorByName('api_key', $this->t('Authentication failed, please check the credentials.'));
    }
  }
  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $triggering_element = $form_state->getTriggeringElement();
    if ($triggering_element['#id'] === 'edit-submit') {
      if (!$form_state->hasValue('account')) {
        $this->config('content_workflow_bynder.settings')
          ->set('username', $form_state->getValue('username'))
          ->set('api_key', $form_state->getValue('api_key'))
          ->save();
        $form_state->setSubmitted()->setRebuild();
      }
      else {
        $submitted_account_id = $form_state->getValue('account');

        $data = $this->client->accountsGet();
        foreach ($data['data'] as $account) {
          if ($account->id == $submitted_account_id) {
            $account_name = $account->name;
            $this->config('content_workflow_bynder.settings')->set('account', serialize([$submitted_account_id => $account_name]))->save();
            $this->messenger()->addMessage($this->t("Credentials and project were saved."));
            $this->config('content_workflow_bynder.settings')->set('content_workflow_bynder_urlkey', $account->slug)->save();
            break;
          }
        }
      }
    }
    elseif ($triggering_element['#id'] === 'edit-reset') {
      $this->config('content_workflow_bynder.settings')->clear('username')->save();
      $this->config('content_workflow_bynder.settings')->clear('api_key')->save();
      $this->config('content_workflow_bynder.settings')->clear('account')->save();
      $this->config('content_workflow_bynder.settings')->clear('content_workflow_bynder_urlkey')->save();
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'content_workflow_bynder.settings',
    ];
  }

}
