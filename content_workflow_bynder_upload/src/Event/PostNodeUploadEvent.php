<?php

namespace Drupal\content_workflow_bynder_upload\Event;

use Symfony\Contracts\EventDispatcher\Event;
use Drupal\Core\Entity\EntityInterface;

/**
 * Wraps a post node upload event for event listeners.
 */
class PostNodeUploadEvent extends Event {

  /**
   * Node object.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected $node;

  /**
   * Source fields.
   *
   * @var array
   */
  protected $contentWorkflowValues;

  /**
   * Constructs a post node upload event object.
   *
   * @param \Drupal\Core\Entity\EntityInterface $node
   *   Entity object.
   * @param array $content_workflow_values
   *   Source fields representing object in ContentWorkflowBynder.
   */
  public function __construct(EntityInterface $node, array $content_workflow_values) {
    $this->node = $node;
    $this->contentWorkflowValues = $content_workflow_values;
  }

  /**
   * Gets the node object.
   *
   * @return \Drupal\node\NodeInterface
   *   The node object.
   */
  public function getNode() {
    return $this->node;
  }

  /**
   * Gets the array of source fields.
   *
   * @return array
   *   Source fields.
   */
  public function getGathercontentValues() {
    return $this->contentWorkflowValues;
  }

}
