<?php

namespace Drupal\content_workflow_bynder_upload\Event;

/**
 * Defines events for the Content Workflow module.
 */
final class ContentWorkflowBynderUploadContentEvents {

  /**
   * Name of the event fired before we post node to ContentWorkflowBynder.
   *
   * This event allows modules to perform an action before node is uploaded
   * from Drupal to ContentWorkflowBynder. The event listener method receives
   * a \Drupal\content_workflow_bynder\Event\PreNodeUploadEvent instance.
   *
   * @Event
   *
   * @see \Drupal\content_workflow_bynder_upload\Event\PreNodeUploadEvent
   *
   * @var string
   */
  const PRE_NODE_UPLOAD = 'content_workflow_bynder.pre_node_upload';

  /**
   * Name of the event fired after we post node to ContentWorkflowBynder.
   *
   * This event allows modules to perform an action after node is uploaded
   * to Content Workflow from Drupal. The event is triggered only after successful
   * upload. The event listener method receives
   * a \Drupal\content_workflow_bynder\Event\PostNodeSaveEvent instance.
   *
   * @Event
   *
   * @see \Drupal\content_workflow_bynder_upload\Event\PostNodeUploadEvent
   *
   * @var string
   */
  const POST_NODE_UPLOAD = 'content_workflow_bynder.post_node_upload';

}
