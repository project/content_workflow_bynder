/**
 * @file
 * Client side manipulation with mapping form.
 */

(function ($) {
  'use strict';
  Drupal.behaviors.contentWorkflowBynderFieldChanged = {
    attach: function (context, settings) {
      $(context).find('.content_workflow_bynder-ct-element').on(
        'change', function () {
          // On select change we want to validate if entity Reference field is selected. If no, we want to hide
          // mapping options, otherwise we want to show them.
          Drupal.contentWorkflowBynder.toggleErMappingType(settings);
        }
      );

      if (typeof settings.content_workflow_bynder !== 'undefined') {
        // On page init we want to hide mapping options, otherwise we want to show them.
        Drupal.contentWorkflowBynder.toggleErMappingType(settings);
      }
    }
  };

  Drupal.contentWorkflowBynder = Drupal.contentWorkflowBynder || {};

  Drupal.contentWorkflowBynder.toggleErMappingType = function (settings) {
    var isErMapped = false;
    $('.content_workflow_bynder-ct-element').each(
      function () {
        if (jQuery.inArray($(this).val(), settings.content_workflow_bynder) !== -1) {
          isErMapped = true;
          return false;
        }
      }
    );

    $('.content_workflow_bynder-er-mapping-type').toggle(isErMapped);
  };

}(jQuery, Drupal));
