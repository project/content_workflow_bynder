/**
 * @file
 */

(function ($, Drupal, once) {

  'use strict';

  Drupal.behaviors.contentWorkflowBynderCounter = {
    attach: function (context) {
      Drupal.contentWorkflowBynder.counterUpdateSelectedMessage();
      $(once('content-workflow-bynder-counter', Drupal.contentWorkflowBynder.counterCountedSelector)).on('change', Drupal.contentWorkflowBynder.onCountedChanged);
    }
  };

  Drupal.contentWorkflowBynder = Drupal.contentWorkflowBynder || {};

  Drupal.contentWorkflowBynder.counterCountedSelector = '.content-workflow-bynder-counted';
  Drupal.contentWorkflowBynder.counterMessageWrapperSelector = '.content-workflow-bynder-counter-message';

  Drupal.contentWorkflowBynder.onCountedChanged = function () {
    Drupal.contentWorkflowBynder.counterUpdateSelectedMessage();
  };

  Drupal.contentWorkflowBynder.counterUpdateSelectedMessage = function () {
    var count = $(Drupal.contentWorkflowBynder.counterCountedSelector + ':checked').length;
    var msg = Drupal.t('There is no selected template');
    if (count !== 0) {
      msg = Drupal.formatPlural(
        count,
        '1 template selected',
        '@count templates selected'
      );
    }

    $(Drupal.contentWorkflowBynder.counterMessageWrapperSelector).text(msg);
  };

})(jQuery, Drupal, once);
