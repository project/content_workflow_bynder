<?php

namespace Drupal\content_workflow_bynder_ui\Form;

use GatherContent\GatherContentClientInterface;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\content_workflow_bynder\DrupalContentWorkflowBynderClient;
use Drupal\content_workflow_bynder\Entity\Mapping;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class MappingImportForm.
 *
 * @package Drupal\content_workflow_bynder\Form
 */
class MappingImportForm extends EntityForm {

  use StringTranslationTrait;

  /**
   * Content Workflow client.
   *
   * @var \Drupal\content_workflow_bynder\DrupalContentWorkflowBynderClient
   */
  protected $client;

  /**
   * MappingImportForm constructor.
   *
   * @param \GatherContent\GatherContentClientInterface $client
   *   GatherContent client.
   */
  public function __construct(GatherContentClientInterface $client) {
    $this->client = $client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('content_workflow_bynder.client')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $account_id = DrupalContentWorkflowBynderClient::getAccountId();
    /** @var \GatherContent\DataTypes\Project[] $projects */
    $projects = [];
    if ($account_id) {
      $projects = $this->client->getActiveProjects($account_id);
    }

    $form['description'] = [
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#value' => $this->t("Please select the Content Workflow Templates you'd like to map. Only Templates you've not selected will be listed."),
      '#attributes' => [
        'class' => ['description'],
      ],
    ];

    $form['projects'] = [
      '#type' => 'vertical_tabs',
    ];

    $form['template_counter'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => [
          'content-workflow-bynder-counter-message',
        ],
      ],
      '#attached' => [
        'library' => [
          'content_workflow_bynder_ui/template_counter',
        ],
      ],
    ];

    $created_mapping_ids = Mapping::loadMultiple();
    $local_templates = [];

    foreach ($created_mapping_ids as $mapping) {
      /** @var \Drupal\content_workflow_bynder\Entity\Mapping $mapping */
      $local_templates[$mapping->getGathercontentTemplateId()] = $mapping->getGathercontentTemplate();
    }

    foreach ($projects['data'] as $project) {
      $remote_templates = $this->client->getTemplatesOptionArray($project->id);
      $templates = array_diff_assoc($remote_templates, $local_templates);

      if (empty($templates)) {
        continue;
      }

      $form['p' . $project->id] = [
        '#type' => 'details',
        '#title' => $project->name,
        '#group' => 'projects',
        '#tree' => TRUE,
      ];
      $form['p' . $project->id]['templates'] = [
        '#type' => 'checkboxes',
        '#title' => $project->name,
        '#options' => $templates,
        '#attributes' => [
          'class' => [
            'content-workflow-bynder-counted',
          ],
        ],
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    if ($form_state->getTriggeringElement()['#id'] == 'edit-submit') {
      // Load all projects.
      $account_id = DrupalContentWorkflowBynderClient::getAccountId();

      if (!$account_id) {
        $this->messenger()->addError($this->t('No available accounts.'));
        $form_state->setRedirect('entity.content_workflow_bynder_mapping.collection');
        return;
      }

      /** @var \GatherContent\DataTypes\Project[] $projects */
      $projects = $this->client->getActiveProjects($account_id);

      $values = $form_state->getValues();

      foreach ($values as $k => $tree) {
        if (!is_array($tree)) {
          continue;
        }
        $templates = array_filter($values[$k]['templates']);
        foreach ($templates as $template_id => $selected) {
          $template = $this->client->templateGet($template_id);
          $templateBody = $this->client->getBody(TRUE);

          $mapping_values = [
            'id' => $template_id,
            'project_id' => $template['data']->projectId,
            'project' => $projects['data'][$template['data']->projectId]->name,
            'template_id' => $template_id,
            'template_name' => $template['data']->name,
            'template' => serialize($templateBody),
          ];

          $mapping = $this->entityTypeManager
            ->getStorage('content_workflow_bynder_mapping')
            ->create($mapping_values);
          if (is_object($mapping)) {
            $mapping->save();
          }
        }
      }
    }

    $form_state->setRedirect('entity.content_workflow_bynder_mapping.collection');
  }

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    $actions = parent::actions($form, $form_state);
    $actions['submit']['#value'] = $this->t('Select');
    $actions['close'] = [
      '#type' => 'submit',
      '#value' => $this->t('Close'),
    ];
    return $actions;
  }

}
