<?php

namespace Drupal\content_workflow_bynder_ui\Form;

use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Builds the form to delete Mapping entities.
 */
class MappingDeleteForm extends EntityConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete %name?',
      [
        '%name' => $this->entity->get('template_name'),
      ],
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.content_workflow_bynder_mapping.collection');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->entity->delete();

    $this->messenger()->addStatus(
      $this->t('Mapping %label has been deleted.',
        [
          '%label' => $this->entity->get('template_name'),
        ],
      )
    );

    $form_state->setRedirectUrl($this->getCancelUrl());
  }

}
