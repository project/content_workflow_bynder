<?php

namespace Drupal\content_workflow_bynder_ui\Controller;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Class TrackedEntitiesController.
 *
 * @package Drupal\content_workflow_bynder_ui\Controller
 */
class TrackedEntitiesController extends ControllerBase {

  /**
   * Query factory instance.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Session service.
   *
   * @var \Symfony\Component\HttpFoundation\Session\Session
   */
  protected $session;

  /**
   * Config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    Session $session,
    EntityTypeManagerInterface $entity_type_manager,
    ConfigFactoryInterface $config
  ) {
    $this->session = $session;
    $this->entityTypeManager = $entity_type_manager;
    $this->config = $config;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('session'),
      $container->get('entity_type.manager'),
      $container->get('config.factory')
    );
  }

  /**
   * Return tracked entities. Entities are added during migration.
   *
   * @return array
   *   Render array.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function listTrackedEntities() {
    $tracked = $this->session->get('content_workflow_bynder_tracked_entities', []);
    $this->session->remove('content_workflow_bynder_tracked_entities');
    $storages = [];
    $rows = [];

    foreach ($tracked as $cwbID => $item) {
      if ($item['entity_type'] === 'paragraph') {
        continue;
      }

      if (!isset($storages[$item['entity_type']])) {
        $storages[$item['entity_type']] = $this->entityTypeManager->getStorage($item['entity_type']);
      }

      /** @var \Drupal\Core\Entity\EntityInterface $entity */
      $entity = $storages[$item['entity_type']]->load($item['id']);
      if (!$entity) {
        if (isset($item['id']) && isset($item['entity_type'])) {
          $messenger = \Drupal::messenger();
          $messenger->addWarning(t('Could not fetch entity @id of type @type.', ['@id' => $item['id'], '@type' => $item['entity_type']]));
        }
        continue;
      }
      $cwbStatus = '';

      if (!empty($item['status'])) {
        $cwbStatus = [
          'data' => [
            'color' => [
              '#type' => 'html_tag',
              '#tag' => 'div',
              '#value' => ' ',
              '#attributes' => [
                'style' => 'width:20px; height: 20px; float: left; margin-right: 5px; background: ' . $item['status']->color,
              ],
            ],
            'label' => [
              '#plain_text' => $item['status']->name,
            ],
          ],
          'class' => ['cwb-item', 'status-item'],
        ];
      }

      $status = '';
      $statusKey = $entity->getEntityType()->getKey('published');

      if (
        $statusKey
        && !$entity->get($statusKey)->isEmpty()
        && $entity->get($statusKey)->first()->getValue()['value']
      ) {
        $status = $this->t('Published');
      }

      $rows[] = [
        'cwb_status' => $cwbStatus,
        'status' => $status,
        'id' => $item['id'],
        'template_name' => $item['template_name'],
        'name' => $entity->label(),
        'drupal_url' => $entity->toLink(),
        'cwb_url' => $this->getCwbUrl($cwbID, $entity->label()),
      ];
    }

    return [
      '#type' => 'table',
      '#header' => [
        'cwb_status' => $this->t('Content Workflow status'),
        'status' => $this->t('Status'),
        'id' => $this->t('ID'),
        'template_name' => $this->t('Template name'),
        'name' => $this->t('Name'),
        'drupal_url' => $this->t('Drupal link'),
        'cwb_url' => $this->t('Content Workflow link'),
      ],
      '#empty' => $this->t('No content available.'),
      '#rows' => $rows,
    ];
  }

  /**
   * Returns the CWB URL for the entity.
   */
  protected function getCwbUrl(int $cwbId, $label) {
    if (is_numeric($cwbId)) {
      $base_url = 'https://'
        . $this->config
          ->get('content_workflow_bynder.settings')
          ->get('content_workflow_bynder_urlkey')
        . '.gathercontent.com/item/';

      $url = Url::fromUri($base_url . $cwbId);

      return Link::fromTextAndUrl($label, $url)->toString();
    }
    else {
      return $this->t('Not available');
    }
  }

}
