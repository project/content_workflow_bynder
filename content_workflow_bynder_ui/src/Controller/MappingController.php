<?php

namespace Drupal\content_workflow_bynder_ui\Controller;

use GatherContent\GatherContentClientInterface;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class MappingController.
 *
 * @package Drupal\content_workflow_bynder\Controller
 */
class MappingController extends ControllerBase {

  /**
   * Content Workflow client.
   *
   * @var \Drupal\content_workflow_bynder\DrupalContentWorkflowBynderClient
   */
  protected $client;

  /**
   * {@inheritdoc}
   */
  public function __construct(GatherContentClientInterface $client) {
    $this->client = $client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('content_workflow_bynder.client')
    );
  }

  /**
   * Page callback for connection testing page.
   *
   * @return array
   *   Content of the page.
   */
  public function testConnectionPage() {
    $message = $this->t('Connection successful.');

    try {
      $this->client->meGet();
    }
    catch (\Exception $e) {
      $message = $this->t("Connection wasn't successful.");
    }

    return [
      '#type' => 'markup',
      '#markup' => $message,
    ];
  }

}
